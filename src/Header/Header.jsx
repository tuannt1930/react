import React from 'react';
import PropTypes from 'prop-types';

Header.propTypes = {
    posts: PropTypes.array,

};
Header.defaultProps = {
    posts: []
}
function Header(props) {
    const { posts } = props;
    return (
        <ul className="postlist">
            {posts.map(post => (
                <li key={post.id}>{post.title}</li>
            ))}
        </ul>
    );
}

export default Header;