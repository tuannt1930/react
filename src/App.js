import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import Header from './Header/Header';
import Pagination from './Pagination';


function App() {
  const [postList, setpostList] = useState([]);

  const [pagination, setpagination] = useState({
    _page: 1,
    _limit: 10,
    _totalRows: 11,
  });

  const [filters, setfilters] = useState({
    _limit: 10,
    _page: 1,
  });

  function handlePageChange(newPage) {
    setfilters({
      ...filters,
      _page: newPage
    })
  }
  useEffect(() => {
    async function fecthPostList() {
      const param = queryString.stringify(filters);
      const requestUrl = `http://js-post-api.herokuapp.com/api/posts?${param}`;
      const response = await fetch(requestUrl);
      const responseJson = await response.json();
      console.log({ responseJson });
      const { data, pagination } = responseJson;
      setpostList(data);
      setpagination(pagination);
    }
    fecthPostList();
  }, [filters]);


  return (
    <div className="App">
      <header className="App-header">
        <h1>hello react</h1>
        <Header posts={postList} />

        <Pagination
          onPagechange={handlePageChange}
          pagination={pagination}
        />
      </header>
    </div>
  );
}

export default App;
