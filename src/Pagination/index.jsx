import React from 'react';
import PropTypes from 'prop-types';

Patination.propTypes = {
    pagination: PropTypes.object.isRequired,
    onPagechange: PropTypes.func,
};
Patination.defaultProps = {
    onPagechange: null
}
function Patination(props) {
    const { pagination, onPagechange } = props;
    const { _page, _limit, _totalRows } = pagination;
    const totalPages = Math.ceil(_totalRows / _limit);
    function handleClick(newPage) {

        if (onPagechange) {
            console.log(newPage);
            onPagechange(newPage);
        }
    }
    return (
        <div>
            <button
                disabled={_page <= 1}
                onClick={() => handleClick(_page - 1)}
            >
                Prev
            </button>

            <button
                disabled={_page >= totalPages}
                onClick={() => handleClick(_page + 1)}
            >
                Next
            </button>
        </div>
    );
}

export default Patination; 